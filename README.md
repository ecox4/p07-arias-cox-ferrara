### HOW TO PLAY ###

* Tap and drag to launch ball
* If you sink the 8 ball before all others that aren't the cue ball, the game will end

### Contributors ###

* Lamar Arias
* Emily Cox
* Dylan Ferrara