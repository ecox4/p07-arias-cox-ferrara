//
//  Universal.m
//  p07-arias-cox-ferrara
//
//  Created by Em on 4/27/17.
//  Copyright © 2017 Em. All rights reserved.
//

#import "Universal.h"

@implementation Universal
@synthesize counter;

static Universal *singleton = nil;
-(id)init{
    if(singleton){
        return singleton;
    }
    self = [super init];
    if(self){
        singleton = self;
    }
    return self;
}

+(Universal *)sharedInstance{
    if(singleton){
        return singleton;
    }
    return [[Universal alloc]init];
}
-(void)saveState{
    NSArray *dirs = [[NSFileManager defaultManager]URLsForDirectory:NSApplicationSupportDirectory inDomains:NSUserDomainMask];
    NSError *err;
    [[NSFileManager defaultManager]createDirectoryAtURL:[dirs objectAtIndex:0] withIntermediateDirectories:YES attributes:nil error:&err];
    NSURL *url = [NSURL URLWithString:@"scoreData.archive" relativeToURL:[dirs objectAtIndex:0]];
    
    NSMutableData *data = [[NSMutableData alloc]init];
    NSKeyedArchiver * archiver = [[NSKeyedArchiver alloc]initForWritingWithMutableData:data];
    [archiver encodeInt:counter forKey:@"counter"];
    [archiver finishEncoding];
    [data writeToURL:url atomically:YES];
    NSLog(@"Score high score %d",counter);
}
-(void)loadState{
    NSArray *dirs = [[NSFileManager defaultManager]URLsForDirectory:NSApplicationSupportDirectory inDomains:NSUserDomainMask];
    NSError *err;
    [[NSFileManager defaultManager]createDirectoryAtURL:[dirs objectAtIndex:0] withIntermediateDirectories:YES attributes:nil error:&err];
    NSURL *url = [NSURL URLWithString:@"scoreData.archive" relativeToURL:[dirs objectAtIndex:0]];
    
    NSData *data = [NSData dataWithContentsOfURL:url];
    if(!data){
        return;
    }
    
    NSKeyedUnarchiver *unarchiver;
    unarchiver = [[NSKeyedUnarchiver alloc]initForReadingWithData:data];
    counter = [unarchiver decodeIntForKey:@"counter"];
    NSLog(@"Loaded high score %d",counter);
}
@end
