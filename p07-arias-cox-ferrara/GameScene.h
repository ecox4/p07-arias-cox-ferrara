//
//  GameScene.h
//  p07-arias-cox-ferrara
//
//  Created by Em on 4/27/17.
//  Copyright © 2017 Em. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "Universal.h"

@interface GameScene : SKScene

@end

static const uint32_t ball = 0x1 <<1;
static const uint32_t pocket = 0x1<<2;
