//
//  Universal.h
//  p07-arias-cox-ferrara
//
//  Created by Em on 4/27/17.
//  Copyright © 2017 Em. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Universal : NSObject

+(Universal *)sharedInstance;

@property (nonatomic) int counter;

-(void)saveState;
-(void)loadState;

@end
