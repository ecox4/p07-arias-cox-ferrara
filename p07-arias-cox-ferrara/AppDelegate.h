//
//  AppDelegate.h
//  p07-arias-cox-ferrara
//
//  Created by Em on 4/27/17.
//  Copyright © 2017 Em. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;


@end

