//
//  GameScene.m
//  p07-arias-cox-ferrara
//
//  Created by Em on 4/27/17.
//  Copyright © 2017 Em. All rights reserved.
//

#import "GameScene.h"

@interface GameScene() <SKPhysicsContactDelegate>

@property BOOL contentCreated;
@property BOOL gameOver;
@property (nonatomic) int score;
@property (nonatomic) int boost;
@property BOOL touching;
@property (nonatomic) NSMutableArray *nballs;

@end

@implementation GameScene{
    //sprite vars
    SKSpriteNode *bg;
    //SKSpriteNode *pouch;
    SKLabelNode *currentScore;
    SKLabelNode *highScore;
    SKSpriteNode *poc1;
    SKSpriteNode *poc2;
    SKSpriteNode *poc3;
    SKSpriteNode *poc4;
    SKSpriteNode *poc5;
    SKSpriteNode *poc6;
    SKSpriteNode *ball0;
    SKSpriteNode *ball1;
    SKSpriteNode *ball2;
    SKSpriteNode *ball3;
    SKSpriteNode *ball4;
    SKSpriteNode *ball5;
    SKSpriteNode *ball6;
    SKSpriteNode *ball7;
    SKSpriteNode *ball8;
    SKSpriteNode *ball9;
    SKSpriteNode *ball10;
    SKSpriteNode *ball11;
    SKSpriteNode *ball12;
    SKSpriteNode *ball13;
    SKSpriteNode *ball14;
    SKSpriteNode *ball15;
    SKSpriteNode *gOverSp;
}

-(void)didMoveToView:(SKView *)view{
    if(!self.contentCreated){
        [self createSceneContents];
        self.contentCreated = YES;
        self.gameOver = NO;
        self.physicsWorld.contactDelegate = self;
        self.backgroundColor = [SKColor greenColor];
        self.score = 0;
        self.boost = 0;
        self.touching = NO;
        
    }
}
-(void)createSceneContents{
    self.scaleMode = SKSceneScaleModeFill;
    self.physicsBody = [SKPhysicsBody bodyWithEdgeLoopFromRect:self.frame];
    self.physicsBody.categoryBitMask = ball;
    //add all children
    [self addChild:[self setBg]];
    [self addChild:[self showCurrentScore]];
    [self addChild:[self showHighScore]];
    
    //make pockets
    //bottom left pocket
    poc1 = [self setPocket:CGPointMake(35, 35) :poc1];
    [self addChild:poc1];
    //bottom right pocket
    poc2 = [self setPocket:CGPointMake(CGRectGetMaxX(self.frame)- 35, 35) :poc2];
    [self addChild:poc2];
    //mid left pocket
    poc3 = [self setPocket:CGPointMake(35,CGRectGetMidY(self.frame)) :poc3];
    [self addChild:poc3];
    //mid right pocket
    poc4 = [self setPocket:CGPointMake(CGRectGetMaxX(self.frame)- 35,CGRectGetMidY(self.frame)) :poc4];
    [self addChild:poc4];
    //top left pocket
    poc5 = [self setPocket:CGPointMake(35,CGRectGetMaxY(self.frame)-35) :poc5];
    [self addChild:poc5];
    //top right pocket
    poc6 = [self setPocket:CGPointMake(CGRectGetMaxX(self.frame)- 35,CGRectGetMaxY(self.frame)-35) :poc6];
    [self addChild:poc6];
    
    //making balls
    //white ball
    SKTexture *que = [SKTexture textureWithImageNamed:@"Billards/Que"];
    que.filteringMode = SKTextureFilteringNearest;
    ball0 = [SKSpriteNode spriteNodeWithTexture:que];
    [ball0 setScale:0.5];
    ball0 = [self setBall:CGPointMake(CGRectGetMidX(self.frame), CGRectGetMaxY(self.frame)/4) :ball0];
    [self addChild:ball0];
    
    [self scoobyDoo];
}
-(void)update:(NSTimeInterval)currentTime{
    //frame-by-frame checks, operations, etc
    if(self.gameOver){
        [self doGameOver];
    }
    if(self.touching){
        //determine speed at which ball is launched, with max speed as mod
        self.boost++;
        self.boost = self.boost % 100;
    }
    currentScore.text = [NSString stringWithFormat:@"Score: %d",self.score];
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    //do powerup stuff, getting ball ready for launch
    //maybe have some form of timer -- int that increments every frame that touch is held
    self.touching = YES;
    if((self.gameOver)){
        [gOverSp removeFromParent];
        self.score = 0;
        [ball4 removeFromParent];
        [self scoobyDoo];
        self.gameOver = NO;
    }
}
-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInNode:self];
    
    double dx = (ball0.position.x - location.x);
    double dy = (ball0.position.y - location.y);
    double dist = sqrt(dx*dx + dy*dy);
    
    if(ball0.physicsBody.velocity.dx == 0 && ball0.physicsBody.velocity.dy ==0){
        
        CGVector vector = CGVectorMake( (location.x - ball0.position.x)*-10, (location.y - ball0.position.y)*-10);
        [ball0.physicsBody applyImpulse:vector];
        
        //need to calculate vector to apply impulse
    }
    
    self.touching = NO;
}
-(void)didBeginContact:(SKPhysicsContact *)contact{
    SKPhysicsBody *body1, *body2;
    if(contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask){
        body1 = contact.bodyA;
        body2 = contact.bodyB;
        //body a is ball
    }else{
        body1 = contact.bodyB;
        body2 = contact.bodyA;
        //body b is ball
    }
    //body 2 is pocket body1 is ball
    SKSpriteNode *tmpb = (SKSpriteNode *)body1.node;
    
    [tmpb removeFromParent];
    
    
    
    
    if(((body1.categoryBitMask & ball) != 0)&&((body2.categoryBitMask & pocket) != 0)){
        SKSpriteNode *node1 = (SKSpriteNode *)body1.node;
        if([node1.name  isEqual: @"8ball"]){
            self.gameOver = YES;
        }else{
            [node1 removeFromParent];
        }
    }
}
-(void)scoobyDoo{
    //(re)making balls
    ball0.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMaxY(self.frame)/4);
    
    //bottom triangle ball
    SKTexture *b1 = [SKTexture textureWithImageNamed:@"Billards/1"];
    b1.filteringMode = SKTextureFilteringNearest;
    ball1 = [SKSpriteNode spriteNodeWithTexture:b1];
    [ball1 setScale:0.5];
    ball1 = [self setBall:CGPointMake(CGRectGetMidX(self.frame), CGRectGetMaxY(self.frame)*(.65)) :ball1];
    [self addChild:ball1];
    
    //left second row ball
    SKTexture *b2 = [SKTexture textureWithImageNamed:@"Billards/2"];
    b2.filteringMode = SKTextureFilteringNearest;
    ball2 = [SKSpriteNode spriteNodeWithTexture:b2];
    [ball2 setScale:0.5];
    ball2 = [self setBall:CGPointMake(CGRectGetMidX(self.frame)-30, CGRectGetMaxY(self.frame)*(.65)+55) :ball2];
    [self addChild:ball2];
    
    //right second row ball
    SKTexture *b3 = [SKTexture textureWithImageNamed:@"Billards/13"];
    b3.filteringMode = SKTextureFilteringNearest;
    ball3 = [SKSpriteNode spriteNodeWithTexture:b3];
    [ball3 setScale:0.5];
    ball3 = [self setBall:CGPointMake(CGRectGetMidX(self.frame)+30, CGRectGetMaxY(self.frame)*(.65)+55) :ball3];
    [self addChild:ball3];
    
    //middle third row ball
    SKTexture *b4 = [SKTexture textureWithImageNamed:@"Billards/8"];
    b4.filteringMode = SKTextureFilteringNearest;
    ball4 = [SKSpriteNode spriteNodeWithTexture:b4];
    [ball4 setScale:0.5];
    ball4 = [self setBall:CGPointMake(CGRectGetMidX(self.frame), CGRectGetMaxY(self.frame)*(.65)+ 105) :ball4];
    ball4.name = @"8ball";
    [self addChild:ball4];
    
    SKTexture *b5 = [SKTexture textureWithImageNamed:@"Billards/15"];
    b5.filteringMode = SKTextureFilteringNearest;
    ball5 = [SKSpriteNode spriteNodeWithTexture:b5];
    [ball5 setScale:0.5];
    ball5 = [self setBall:CGPointMake(CGRectGetMidX(self.frame) - 60, CGRectGetMaxY(self.frame)*(.65)+ 105) :ball5];
    [self addChild:ball5];
    
    SKTexture *b6 = [SKTexture textureWithImageNamed:@"Billards/6"];
    b6.filteringMode = SKTextureFilteringNearest;
    ball6 = [SKSpriteNode spriteNodeWithTexture:b6];
    [ball6 setScale:0.5];
    ball6 = [self setBall:CGPointMake(CGRectGetMidX(self.frame) +60, CGRectGetMaxY(self.frame)*(.65)+ 105) :ball6];
    [self addChild:ball6];
    
    //fourth row
    SKTexture *b7 = [SKTexture textureWithImageNamed:@"Billards/10"];
    b7.filteringMode = SKTextureFilteringNearest;
    ball7 = [SKSpriteNode spriteNodeWithTexture:b7];
    [ball7 setScale:0.5];
    ball7 = [self setBall:CGPointMake(CGRectGetMidX(self.frame) -45, CGRectGetMaxY(self.frame)*(.65)+ 155) :ball7];
    [self addChild:ball7];
    
    SKTexture *b8 = [SKTexture textureWithImageNamed:@"Billards/4"];
    b8.filteringMode = SKTextureFilteringNearest;
    ball8 = [SKSpriteNode spriteNodeWithTexture:b8];
    [ball8 setScale:0.5];
    ball8 = [self setBall:CGPointMake(CGRectGetMidX(self.frame) +45, CGRectGetMaxY(self.frame)*(.65)+ 155) :ball8];
    [self addChild:ball8];
    
    SKTexture *b9 = [SKTexture textureWithImageNamed:@"Billards/9"];
    b9.filteringMode = SKTextureFilteringNearest;
    ball9 = [SKSpriteNode spriteNodeWithTexture:b9];
    [ball9 setScale:0.5];
    ball9 = [self setBall:CGPointMake(CGRectGetMidX(self.frame) +75, CGRectGetMaxY(self.frame)*(.65)+ 155) :ball9];
    [self addChild:ball9];
    
    SKTexture *b10 = [SKTexture textureWithImageNamed:@"Billards/3"];
    b10.filteringMode = SKTextureFilteringNearest;
    ball10 = [SKSpriteNode spriteNodeWithTexture:b10];
    [ball10 setScale:0.5];
    ball10 = [self setBall:CGPointMake(CGRectGetMidX(self.frame) -75, CGRectGetMaxY(self.frame)*(.65)+ 155) :ball10];
    [self addChild:ball10];
    
    //fifth row
    SKTexture *b11 = [SKTexture textureWithImageNamed:@"Billards/11"];
    b11.filteringMode = SKTextureFilteringNearest;
    ball11 = [SKSpriteNode spriteNodeWithTexture:b11];
    [ball11 setScale:0.5];
    ball11 = [self setBall:CGPointMake(CGRectGetMidX(self.frame), CGRectGetMaxY(self.frame)*(.65)+ 205) :ball11];
    [self addChild:ball11];
    
    SKTexture *b12 = [SKTexture textureWithImageNamed:@"Billards/5"];
    b12.filteringMode = SKTextureFilteringNearest;
    ball12 = [SKSpriteNode spriteNodeWithTexture:b12];
    [ball12 setScale:0.5];
    ball12 = [self setBall:CGPointMake(CGRectGetMidX(self.frame) -60, CGRectGetMaxY(self.frame)*(.65)+ 205) :ball12];
    [self addChild:ball12];
    
    SKTexture *b13 = [SKTexture textureWithImageNamed:@"Billards/7"];
    b13.filteringMode = SKTextureFilteringNearest;
    ball13 = [SKSpriteNode spriteNodeWithTexture:b13];
    [ball13 setScale:0.5];
    ball13 = [self setBall:CGPointMake(CGRectGetMidX(self.frame) +60, CGRectGetMaxY(self.frame)*(.65)+ 205) :ball13];
    [self addChild:ball13];
    
    SKTexture *b14 = [SKTexture textureWithImageNamed:@"Billards/14"];
    b14.filteringMode = SKTextureFilteringNearest;
    ball14 = [SKSpriteNode spriteNodeWithTexture:b14];
    [ball14 setScale:0.5];
    ball14 = [self setBall:CGPointMake(CGRectGetMidX(self.frame) -120, CGRectGetMaxY(self.frame)*(.65)+ 205) :ball14];
    [self addChild:ball14];
    
    SKTexture *b15 = [SKTexture textureWithImageNamed:@"Billards/12"];
    b15.filteringMode = SKTextureFilteringNearest;
    ball15 = [SKSpriteNode spriteNodeWithTexture:b15];
    [ball15 setScale:0.5];
    ball15 = [self setBall:CGPointMake(CGRectGetMidX(self.frame) +120, CGRectGetMaxY(self.frame)*(.65)+ 205) :ball15];
    [self addChild:ball15];
}

-(void)doGameOver{
    [self setBest];
    NSString *goSprite;
    if(self.only8){
        goSprite = @"winner_sprite.png";
    }else{
        goSprite = @"loser_sprite.png";
    }
    gOverSp = [SKSpriteNode spriteNodeWithImageNamed:goSprite];
    gOverSp.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
    gOverSp.size = CGSizeMake(300, 500);
    gOverSp.name = @"potato";
    [self addChild:gOverSp];
    
}
-(BOOL)only8{
    if((!ball1.parent)&&(!ball2.parent)&&(!ball3.parent)&&(ball4.parent)&&(!ball5.parent)&&(!ball6.parent)&&(!ball7.parent)&&(!ball8.parent)&&(!ball9.parent)&&(!ball10.parent)&&(!ball11.parent)&&(!ball12.parent)&&(!ball13.parent)&&(!ball14.parent)&&(!ball15.parent)){
        return YES;
    }else{
        return NO;
    }
}
//sprite creation fns
-(SKSpriteNode *)setBg{
    bg = [SKSpriteNode spriteNodeWithImageNamed:@"green_felt.jpg"];
    bg.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
    bg.name = @"bg";
    return bg;
}
-(SKSpriteNode *)setPocket:(CGPoint)pt :(SKSpriteNode *)pouch{
    //make ctor
    pouch = [SKSpriteNode spriteNodeWithImageNamed:@"pocket.png"];
    pouch.size = CGSizeMake(90, 90);
    pouch.physicsBody = [SKPhysicsBody bodyWithTexture:pouch.texture size:pouch.size];
    pouch.physicsBody.categoryBitMask = pocket;
    pouch.physicsBody.contactTestBitMask = ball;
    pouch.physicsBody.affectedByGravity = NO;
    pouch.physicsBody.dynamic = NO;
    pouch.position = pt;
    //pouch.name = name;
    return pouch;
}

-(SKSpriteNode *) setBall:(CGPoint)spot :(SKSpriteNode *)balls{
    //have to replace with actual images
    balls.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:balls.frame.size.width/2];
    balls.physicsBody.categoryBitMask = ball;
    balls.physicsBody.contactTestBitMask = pocket;
    balls.physicsBody.affectedByGravity = NO;
    balls.physicsBody.dynamic = YES;
    balls.position = spot;
    return balls;
}

-(SKLabelNode *)showCurrentScore{
    currentScore = [SKLabelNode labelNodeWithFontNamed:@"Courier"];
    currentScore.text = [NSString stringWithFormat:@"Score: %d",self.score];
    currentScore.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMinY(self.frame)+30);
    currentScore.fontSize = 28;
    currentScore.fontColor = [SKColor whiteColor];
    currentScore.name = @"scoreLabel";
    return currentScore;
}
-(SKLabelNode *)showHighScore{
    highScore = [SKLabelNode labelNodeWithFontNamed:@"Courier"];
    highScore.text = [NSString stringWithFormat: @"Best: %d",[[Universal sharedInstance] counter]];
    highScore.position = CGPointMake(CGRectGetMidX(self.frame),CGRectGetMaxY(self.frame)-50);
    highScore.fontSize = 28;
    highScore.fontColor = [SKColor whiteColor];
    highScore.name = @"hiScore";
    return highScore;
}

-(void)setBest{
    if(self.score > [[Universal sharedInstance] counter]){
        [[Universal sharedInstance]setCounter:self.score];
    }
}
@end
